typedef struct node{
	char c;
	struct node *next, *prev;
}node;

typedef struct line{
	node *head, *tail;
}line;

/* Initialize the line
 */

void initLine(line *l);

/* create lines from the given file.
 * argument: file descriptor fd(int)
 * return value : pointer to an array of lines created
 */

void printLine(line);

/* Search string in the given line
 * argument : string and line
 * return value : int(string present/absent in the line)
 */

void appendChar(line *, char);

/* free up the line
 */

void destroyLine(line *);

/* searches the character pattern in the given line
 * argument: line and string
 * return value : int(bool)
 */

int lstrstr(line, char *);

/* Searches the pattern in the given line ignoring the case
 */

int lcasestrstr(line, char *);

/* converts a single character to a string
 */

char * strconv(char);