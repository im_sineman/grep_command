Name: Gaurav Deore
MIS: 111603012
Topic: Grep Command
Description:
	-The project searches for a given word in the given files and prints/counts the number of lines in which the word is found. 
	-The options -c,-v and -i have been implemented along with all their permutations.
	-The command to be fed has to be in the order : ./<executable> -<options> <pattern> <filename> <filename> ...
	