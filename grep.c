#define PRINT 100
#define COUNT 101
#define STATUS_DEFAULT PRINT
#define MATCH 200
#define UNMATCH 201
#define TYPE_DEFAULT MATCH
#define YES 1
#define NO 0
#define SENSITIVE 300
#define INSENSITIVE 301
#define CASE_DEFAULT SENSITIVE

#define CYAN    "\x1b[36m"

#define RESET   "\x1b[0m"

typedef struct mode{
	int status; /*print or count*/
	int Case; /*case sensitive or insensitive*/
	int type; /*whether to match or unmatch*/
	int printfilename;
	int recursion;
}mode;


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <strings.h>


#include "line.h"


mode mode_check(char *);
void grep_basic(char *, char *, mode);


int main(int a, char *args[]){
	int i = 0, fd, n, j = 0, k = 0;
	line l;
	char ch;
	mode m;

	if(a < 3){
		printf("Usage: ./grep [OPTION]... PATTERN [FILE]...\nTry 'grep --help' for more information.\n");
		return 0;
	}
	else if(args[1][0] != '-'){
		i = 2;
		while(i < a){
			m.status = STATUS_DEFAULT;
			m.type = TYPE_DEFAULT;
			if(a > 3)
				m.printfilename = YES;
			else
				m.printfilename = NO;
			m.Case = SENSITIVE;
			grep_basic(args[i], args[1], m);
			i++;
		}
	}
	else if(args[1][0] == '-'){
		i = 3;
		while(i < a){
			m = mode_check(args[1]);
		//	printf("m.Case = %d\n", m.Case);
			if(a > 4)
				m.printfilename = YES;
			else 
				m.printfilename = NO;
			grep_basic(args[i], args[2], m);
			i++;
			
		}
	}

	return 0;
}

void grep_basic(char *filename, char *pattern, mode m){
	int fd, n, k, count = 0;
	line l;
	char ch;

	fd = open(filename, O_RDONLY);
	if(fd == -1){
		printf("Unable to open file\n");
		return;
	}
	initLine(&l);
	k = read(fd, &ch, sizeof(char));
	while(k != 0){
		if(ch != '\n'){
			appendChar(&l ,ch);
		}
		else{
			if(m.Case == SENSITIVE){
				n = lstrstr(l, pattern);
		//		printf("wels\n");

			}
			else if(m.Case == INSENSITIVE){
				n = lcasestrstr(l, pattern);
		//		printf("wer\n");

			}
			if((m.type == MATCH) && (n == 1)){
				if(m.status == PRINT){
					if(m.printfilename == YES){
						//textcolor(VOILET);
						printf(CYAN "%s: " RESET, filename);	
					}
					printLine(l);
				}
				else if(m.status == COUNT){	
					count++;
				}
			}
			else if((m.type == UNMATCH) && (n == 0)){
				if(m.status == PRINT){
					if(m.printfilename == YES)
						printf(CYAN "%s: " RESET, filename);	
					printLine(l);
				}
				else if(m.status == COUNT){
					count++;
				}	
			}
			//printf("asd\n");
			destroyLine(&l);
			initLine(&l);
		}
		k = read(fd, &ch, sizeof(char));
	//	printf("k = %d\n", k);
	}
	//printf("qw\n");
	if(m.status == COUNT){
		if(m.printfilename == YES)
			printf(CYAN "%s: " RESET, filename);	
		printf("%d\n", count);
	}
}

mode mode_check(char *ch){
	int i = 0, j = 1;
	mode m;
	char arr[] = {'v', 'c', 'i', '\0'};
	while(arr[i] != '\0'){
		j = 1;
		while(ch[j] != '\0'){
			if(arr[i] == ch[j]){
				if(i == 0){
					m.type = UNMATCH;
				}
				else if(i == 1){
					m.status = COUNT;
				}
				else if(i == 2){
			//		printf("wer\N");
					m.Case = INSENSITIVE;
			
		//			printf("m.Case = %d\n%d", m.Case, INSENSITIVE);
				}
				j = 1;
				break;
			}
			else 
				j++;
		}
		i++;
	}
	if(m.status != PRINT && m.status != COUNT)
		m.status = STATUS_DEFAULT;
	if(m.type != MATCH && m.type != UNMATCH)
		m.type = TYPE_DEFAULT;
	if(m.Case != INSENSITIVE)
		m.Case = SENSITIVE;

	return m;
}