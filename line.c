#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "line.h"

void initLine(line *l){
	l->head = NULL;
	l->tail = NULL;
}

void printLine(line l){
	node *current;
	current = l.head;
	if(l.head == NULL){
		printf(" ");
	}	
	else {
		printf("%c", current->c);
		current = current->next;
		while(current != l.head){
			printf("%c", current->c);
			current = current->next;
	}
	printf("\n");

		
	}
	return;
}

void appendChar(line *l, char ch){
	if(l->head == NULL){
		l->head = (node *)malloc(sizeof(node));
		l->head->c = ch;
		l->tail = l->head;
		l->head->next = l->head;
		l->head->prev = l->head;
	}
	else {
		l->tail->next = (node *)malloc(sizeof(node));
		l->tail->next->c = ch;
		l->tail->next->prev = l->tail;
		l->tail = l->tail->next;
		l->tail->next = l->head;
		l->head->prev = l->tail;
	}
}

void destroyLine(line *l){
	while(l->tail != l->head){	
		l->tail = l->tail->prev;
		free(l->tail->next);	
	}
	free(l->head);
	l->tail = NULL;	
	l->head = NULL;
}

int lstrstr(line l, char *ch){
	int i = 0;
	node *current;
	if(l.head == NULL)
		return 0;
	current = l.head;
	if(current->c == ch[i])
		i++;
	current = current->next;
//	printf("0\n");
	while((current != l.head) && (ch[i] != '\0')){
		if(current->c == ch[i]){
			i++;
		//	printf("1\n");
			current = current->next;
		//	sleep(1);
		}
		else{
			if(i != 0)
				i = 0;
			else
				current = current->next;
		}
	}

//	if(current == l.head)
//		printf("sdf");
	if(ch[i] == '\0'){
//		printf("asd\n");
		return 1;
	}
	else 
		return 0;
}

int lcasestrstr(line l, char *ch){
	int i = 0;
	node *current;
	if(l.head == NULL)
		return 0;
	current = l.head;
	if(!strcasecmp(strconv(current->c), strconv(ch[i])))
		i++;
	current = current->next;
//	printf("0\n");
	while((current != l.head) && (ch[i] != '\0')){
		if(!strcasecmp(strconv(current->c), strconv(ch[i]))){
			i++;
		//	printf("1\n");
			current = current->next;
		//	sleep(1);
		}
		else{
			if(i != 0)
				i = 0;
			else
				current = current->next;
		}
	}

//	if(current == l.head)
//		printf("sdf");
	if(ch[i] == '\0'){
//		printf("asd\n");
		return 1;
	}
	else 
		return 0;
}

char * strconv(char c){
	char *ch = (char *)malloc(sizeof(char) * 2);
	ch[0] = c;
	ch[1] = 'b';
	return ch;
}